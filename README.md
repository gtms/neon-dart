# Neon - dart
[neon javascript library](https://github.com/matej21/neon-js) ported to dart.

## This project is WIP
Lots of tests are not handled. Some results are not stable. 
**Currently working on proper return value from decode function.**

# Installation

You need to import [javascript library](https://github.com/matej21/neon-js) to page. Then you can use this package in your dart app.

# API
_Trying to mirror behavior from matej21._

## decode (input)
```dart
NMap result = decode(input);
```
## encode (var [, options])
```dart
Map data = {'foo': 'bar'};
String result = encode(data);
```
