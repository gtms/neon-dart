@JS('neon')
library neon;

import 'package:js/js.dart' show JS, anonymous;
import 'package:js/js_util.dart' show getProperty, jsify;
import 'convert.dart';

external dynamic get BLOCK;
external dynamic get CHAIN;
@JS('decode')
external dynamic _decode(String input);
dynamic decode(String input) {
  final result = _decode(input);
  if (getProperty(result, 'add') != null) return jsObjectToMap(result.toObject());
  return result;
}

@JS('encode')
external String _encode(dynamic data, [int options]);
String encode(dynamic data, [int options]) {
  if (data is Map) return _encode(jsify(data), options);
  return _encode(data, options);
}

@JS('Map')
class NMap {
  external int get length;

  @JS()
  external NMap();
  @JS('fromObject')
  external static NMap _fromObject(Object obj);
  static NMap fromObject(Map map) => _fromObject(jsify(map));
  external static NMap fromArray(List list);

  external void set(dynamic key, dynamic value, [bool replace = true]);
  external void add(String key, dynamic value);
  external dynamic get(String key);
  external dynamic last();
  external bool has(dynamic key);
  external void remove(dynamic key);
  external List<NeonProperties> items();
  external List keys();
  external List values();
  external dynamic toObject();
  external void forEach(void f(key, value));
  external bool isList();
}

@JS()
@anonymous
class Entity {
  external Entity([dynamic value, dynamic attributes]);
  external String get value;
  external NMap get attributes;
  external set value(String value);
  external set attributes(dynamic attrs);
}

@JS()
class Dumper {
  external static toText(NMap neonMap);
}

@JS()
@anonymous
class Error {
  external Error(String message, [line, column]);
  external String get name;
  external String get message;
  external dynamic get line;
  external dynamic get column;
  external set message(String value);
  external set line(value);
  external set column(value);
}

@JS()
@anonymous
class NeonProperties {
  external dynamic get key;
  external dynamic get value;
}
