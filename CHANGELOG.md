# 0.0.5
- Adding test cases
- Converting map to js object for `encode` and `NMap.fromObject`
- Complete API for `NMap`
- Add `Entity`
- Add `Error`
# 0.0.4
- Fixing README
# 0.0.3
- Adding testing cases
- learning to publish package to pub.dev
# 0.0.2
- Add constraints and important files
# 0.0.1
- Initial commit
