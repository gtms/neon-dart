@TestOn('browser')

import 'package:test/test.dart';
import 'package:nette_neon/nette_neon.dart' as neon;

void main() {
  neon.NMap easy;

  setUp(() {
    easy = neon.decode(_easyConfig);
  });

  test('test 1', () {
    print(easy.runtimeType);
    print(easy is neon.NMap);
    print(easy.get('key'));
    print(easy.length);
  });

  test('test 2', () {
    final ent = neon.decode('Foo(bar)');
    print(ent.value);
    print(neon.Dumper.toText(ent.attributes));
  });
}

const String _easyConfig = r'''
key: value
list: [a, b, c]
''';
