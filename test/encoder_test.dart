@TestOn('browser')

import 'package:test/test.dart';
import 'package:nette_neon/nette_neon.dart' as neon;

void main() {
  test('true, false, null constants and strings', () {
    final map = neon.encode([
      true,
      'TRUE',
      'tRuE',
      'true',
      false,
      'FALSE',
      'fAlSe',
      'false',
      null,
      'NULL',
      'nUlL',
      'null',
      'yes',
      'no',
      'on',
      'off'
    ]);

    expect(
        map,
        equals(
            '[true, "TRUE", "tRuE", "true", false, "FALSE", "fAlSe", "false", null, "NULL", "nUlL", "null", "yes", "no", "on", "off"]'));
  });

  test('numbers', () {
    expect(neon.encode([1, 1.0, 0, 0.0, -1, -1.2, '1', '1.0', '-1']), '[1, 1, 0, 0, -1, -1.2, "1", "1.0", "-1"]');
  });

  test('symbols', () {
    expect(neon.encode(['[', ']', '{', '}', ':', ': ', '=', '#']), '["[", "]", "{", "}", ":", ": ", "=", "#"]');
  });

  test('list', () {
    expect(neon.encode([1, 2, 3]), '[1, 2, 3]');
  });

  test('object', () {
    expect(neon.encode({1: 1, 2: 2, 3: 3}), '{"1": 1, "2": 2, "3": 3}');
  });

  test('list with missing key', () {
    var arr = []..length = 4;
    arr[1] = 1;
    arr[2] = 2;
    arr[3] = 3;
    expect(neon.encode(arr), '{1: 1, 2: 2, 3: 3}');
  });

  test('object and array', () {
    expect(
        neon.encode({
          'foo': 1,
          'bar': [2, 3]
        }),
        '{foo: 1, bar: [2, 3]}');
  });

  test('map with list', () {
    expect(neon.encode(neon.NMap.fromArray([1, 2, 3])), '[1, 2, 3]');
  });

  test('map with assoc array', () {
    expect(neon.encode(neon.NMap.fromObject({1: 1, "foo": 2})), '{"1": 1, foo: 2}');
  });

  test('map', () {
    var map = new neon.NMap();
    map.set(1, 1);
    map.set("foo", 2);
    expect(neon.encode(map), '{1: 1, foo: 2}');
  });

  test('entity 1', () {
    expect(neon.encode(neon.decode('item(a, b)')), 'item(a, b)');
  });

  test('entity 2', () {
    expect(neon.encode(neon.decode('item<item>(a, b)')), 'item<item>(a, b)');
  });

  test('entity 3', () {
    expect(neon.encode(neon.decode('item(foo: a, bar: b)')), 'item(foo: a, bar: b)');
  });

  test('entity 4', () {
    expect(neon.encode(neon.decode('[]()')), '[]()');
  });

  test('entity 5', () {
    expect(neon.encode(neon.decode('item(a, foo: b)')), 'item(0: a, foo: b)');
  });

  test('entity 6', () {
    var entity = new neon.Entity("ent");
    entity.attributes = null;
    expect(neon.encode(entity), 'ent()');
  });

  test('block', () {
    expect(neon.encode(["foo", "bar"], neon.BLOCK), "- foo\n" + "- bar\n");
  });

  test('block 2', () {
    expect(neon.encode({'x': "foo", 'y': "bar"}, neon.BLOCK), "x: foo\n" + "y: bar\n");
  });

  test('block 3', () {
    expect(
        neon.encode({
          'x': "foo",
          'y': [1, 2]
        }, neon.BLOCK),
        "x: foo\n" + "y:\n" + "	- 1\n" + "	- 2\n");
  });

  test('block 5', () {
    expect(
        neon.encode({
          'x': "foo",
          'y': [1, 2]
        }, neon.BLOCK),
        "x: foo\n" + "y:\n" + "	- 1\n" + "	- 2\n");
  });

  test('block 6', () {
    expect(
        neon.encode({
          'a': {
            'foo1': {'lorem': 1},
            'foo2': {'lorem': 2}
          }
        }, neon.BLOCK),
        "a:\n" + "	foo1:\n" + "		lorem: 1\n" + "	foo2:\n" + "		lorem: 2\n");
  });

  test('sentence', () {
    expect(neon.encode("Sed tempus eu tortor sagittis commodo. Phasellus luctus pharetra lectus, at vulputate ex."),
        "\"Sed tempus eu tortor sagittis commodo. Phasellus luctus pharetra lectus, at vulputate ex.\"");
  });

  test('sentence no comma', () {
    expect(neon.encode("Sed tempus eu tortor sagittis commodo. Phasellus luctus pharetra lectus at vulputate ex."),
        "Sed tempus eu tortor sagittis commodo. Phasellus luctus pharetra lectus at vulputate ex.");
  });
}
