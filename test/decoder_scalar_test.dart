@TestOn('browser')

import 'package:test/test.dart';
import 'package:nette_neon/nette_neon.dart' as neon;

void main() {
  test('null', () {
    expect(neon.decode(''), null);
  });

  test('null 2', () {
    expect(neon.decode(' '), null);
  });

  test('int 0', () {
    expect(neon.decode('0'), 0);
  });

  test('float 0.0 (as int)', () {
    expect(neon.decode('0.0'), 0);
  });

  test('int 1', () {
    expect(neon.decode('1'), 1);
  });

  test('float -1.2', () {
    expect(neon.decode('-1.2'), -1.2);
  });

  test('float -1.2e2', () {
    expect(neon.decode('-1.2e2'), -120);
  });

  test('true', () {
    expect(neon.decode('true'), true);
  });

  test('false', () {
    expect(neon.decode('false'), false);
  });

  test('null string', () {
    expect(neon.decode('null'), null);
  });

  test('literal 1', () {
    expect(neon.decode('the"string#literal'), 'the"string#literal');
  });

  test('literal 2', () {
    expect(neon.decode('the"string #literal'), 'the"string');
  });

  test('literal 3', () {
    expect(neon.decode('"the\'string #literal"'), "the'string #literal");
  });

  test('literal 4', () {
    expect(neon.decode("'the\"string #literal'"), 'the"string #literal');
  });

  test('literal 5', () {
    expect(neon.decode('"the\\"string #literal"'), 'the"string #literal');
  });

  test('literal 5', () {
    expect(neon.decode("<literal> <literal>"), "<literal> <literal>");
  });

  test('empty string 1', () {
    expect(neon.decode("''"), "");
  });

  test('empty string 2', () {
    expect(neon.decode('""'), "");
  });

  test('string :a', () {
    expect(neon.decode(':a'), ":a");
  });

  test('char x', () {
    expect(neon.decode('x'), "x");
  });

  test('char x 2', () {
    expect(neon.decode('\nx\n'), "x");
  });

  test('char x 3', () {
    expect(neon.decode(' x'), "x");
  });

  test('@x', () {
    expect(neon.decode('@x'), "@x");
  });

  test('@true', () {
    expect(neon.decode('@true'), "@true");
  });

  // test('date', () {
  //   expect(neon.decode('2014-05-20'), new DateTime(2014, 5, 20));
  // });

  test('spaaace', () {
    expect(neon.decode('a                     '), "a");
  });

  test('BOM!', () {
    expect(neon.decode('\xEF\xBB\xBFa'), "a");
  });

  test('unicode', () {
    expect(neon.decode('"\\u0040"'), '@');
    expect(neon.decode('"\\u011B"'), "\u011B");
    expect(neon.decode('"\\uD834\\uDF06"'), '\uD834\uDF06');
  });
}
